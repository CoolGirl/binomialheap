module BinomialHeap where

import           Data.Either.Extra (fromRight)

data BinTree a = Leaf | Node a Int [BinTree a]
    deriving (Eq, Ord, Show)

type BinHeap a = [BinTree a]

combine :: (Ord a) => BinTree a -> BinTree a -> Either String (BinTree a)
combine Leaf tree2 = Right tree2
combine tree Leaf  = Right tree
combine tree tree2 = mergeTrees tree tree2

mergeTrees :: (Ord a) => BinTree a -> BinTree a -> Either String (BinTree a)
mergeTrees (Node value rank subTreesList) (Node value2 rank2 subTreesList2) | rank == rank2 =
    Right (if value <= value2 then Node value (rank + 1) (Node value2 rank2 subTreesList2 : subTreesList)
        else Node value2 (rank2 + 1) (Node value rank subTreesList : subTreesList2))
mergeTrees _ _ = Left "Binomial trees with a different rank cannot be merged."

mergeHeaps :: (Ord a) => BinHeap a -> BinHeap a -> BinHeap a
mergeHeaps [] heap2   = heap2
mergeHeaps heap []    = heap
mergeHeaps heap heap2 = mergeHeapsSafe heap heap2

mergeHeapsSafe :: (Ord a) => BinHeap a -> BinHeap a -> BinHeap a
mergeHeapsSafe heap1@(tree1@(Node _ rank _):tail1) heap2@(tree2@(Node _ rank2 _):tail2)
    | rank < rank2 = tree1 : mergeHeaps tail1 heap2
    | rank2 < rank = tree2 : mergeHeaps tail2 heap1
    | otherwise = mergeHeaps [fromRight (combine tree1 tree2)] (tail1 ++ tail2)

insert :: (Ord a) => a -> BinHeap a -> BinHeap a
insert newValue = mergeHeaps (singleton newValue)

extractMin :: (Ord a) => BinHeap a -> Either String a
extractMin [] = Left "Cannot extract minimum from an empty heap."
extractMin [Node value _ _] = Right value
extractMin (Node value _ _ : tail) = Right $ min value (fromRight (extractMin tail))

deleteMin :: (Ord a) => BinHeap a -> Either String (BinHeap a, a)
deleteMin [] = Left "Cannot delete from an empty heap."
deleteMin heap = let minValue = fromRight (extractMin heap) in
    Right (deleteMin2 minValue heap [], minValue)

deleteMin2 :: (Ord a) => a -> BinHeap a -> BinHeap a -> BinHeap a
deleteMin2 minValue (x@(Node value _ subtree):tail) init = if value == minValue
    then mergeHeaps (reverse subtree) (reverse init ++ tail) else deleteMin2 minValue tail (x:init)

fromList :: (Ord a) => [a] -> Either String (BinHeap a)
fromList []       = Left "Cannot make a binomial heap from an empty list."
fromList [x]      = Right $ singleton x
fromList (x:tail) = Right $ fromList2 (singleton x) tail

fromList2 :: (Ord a) => BinHeap a -> [a] -> BinHeap a
fromList2 heap [x]      = mergeHeaps heap (singleton x)
fromList2 heap (x:tail) = fromList2 (mergeHeaps heap (singleton x)) tail

singleton :: a -> BinHeap a
singleton x = [Node x 0 []]

toList :: (Ord a) => BinHeap a -> Either String [a]
toList [] = Right []
toList heap = let (newHeap, minValue) = fromRight (deleteMin heap) in
    Right (minValue : fromRight (toList newHeap))
